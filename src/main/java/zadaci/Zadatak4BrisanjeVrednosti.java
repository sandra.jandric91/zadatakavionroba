package zadaci;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.stmt.query.In;
import com.j256.ormlite.support.ConnectionSource;
import model.Avion;
import model.Roba;

import java.io.IOException;
import java.util.List;

public class Zadatak4BrisanjeVrednosti {

    static Dao<Avion, Integer> avionDao;
    static Dao<Roba, Integer> robaDao;

    public static void main(String[] args) {

        ConnectionSource connectionSource = null;

        try {

            connectionSource = new JdbcConnectionSource("jdbc:sqlite:avionRoba.db");
            avionDao = DaoManager.createDao(connectionSource, Avion.class);
            robaDao = DaoManager.createDao(connectionSource, Roba.class);


            List<Roba> roba = robaDao.queryForAll();
            for (Roba r : roba)
                System.out.println("Roba = " + r);
            Roba robaZaBrisanje = robaDao.queryForEq(Roba.POLJE_NAZIV, "Voda").get(0);
            robaDao.delete(robaZaBrisanje);

            roba = robaDao.queryForAll();
            for(Roba r : roba)
                System.out.println("Roba = " + r);



        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connectionSource != null) {
                    connectionSource.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
