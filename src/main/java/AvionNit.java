import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import model.Avion;
import model.Roba;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AvionNit extends Thread {

    static Dao<Avion, Integer> avionDao;
    static Dao<Roba, Integer> robaDao;


    public Avion avion;
    public static Boolean dozvoljenoPoletanje = true;
    public static Object sinhronizacija = new Object();


    public AvionNit(Avion avion) {
        this.avion = avion;
    }

    public void run() {

        System.out.println("Pocinje provera za avion " + avion);
        int period = (int)(Math.random() * 2000);
        try {
            sleep(period);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Avion " + avion + " je spreman za poletanje  i ceka dozvolu za poletanje");

        boolean pistaSlobodna;
        do {
            synchronized (sinhronizacija) {
                if (dozvoljenoPoletanje) {
                    dozvoljenoPoletanje = false;
                    pistaSlobodna = true;
                } else {
                    pistaSlobodna = false;
                }
            }
            if (pistaSlobodna == false) {
                try {
                    sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } while (!pistaSlobodna);

        System.out.println("Avion " + avion + " izlazi na pistu i polece");
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Avion " + avion + " je poleteo");

        synchronized (sinhronizacija) {
            dozvoljenoPoletanje = true;
        }


    }

    public static void main(String[] args) {

        ConnectionSource connectionSource = null;

        try {

            connectionSource = new JdbcConnectionSource("jdbc:sqlite:avionRoba.db");
            avionDao = DaoManager.createDao(connectionSource, Avion.class);
            robaDao = DaoManager.createDao(connectionSource, Roba.class);


            ArrayList<Thread> tredovi = new ArrayList<>();
            List<Avion> avioni = avionDao.queryForAll();
            for (Avion a : avioni) {
                AvionNit an = new AvionNit(a);
                tredovi.add(an);
                an.start();
            }


            for (Thread t : tredovi) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            System.out.println("Svi avioni su poleteli");


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
            try {
                if (connectionSource != null) {
                    connectionSource.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}


